﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class Slingshot : MonoBehaviour {

    public float minStretch;
    public float maxStretch;
    public LineRenderer lineRender;
    public Transform lineRendererStart;

    private Vector3[] tempArray;
    private SpringJoint2D springElem;
    private Rigidbody2D rigid;
    private bool canDrag = false;
    private Vector2 oldVelocity;
	// Use this for initialization
	void Start () {
        tempArray = new Vector3[2];
        springElem = GetComponent<SpringJoint2D>();
        rigid = GetComponent<Rigidbody2D>();
        setLineRenderer(false);
	}
	
	// Update is called once per frame
	void Update () {

        if(canDrag)
        {
            DoDrag();
        }


        if(springElem!=null)
        {
            if(rigid.isKinematic==false && oldVelocity.sqrMagnitude>rigid.velocity.sqrMagnitude)
            {
                Destroy(springElem);
                rigid.velocity = oldVelocity;
            }

            setLineRenderer(false);

            if(canDrag==false)
            {
                oldVelocity = rigid.velocity;
            }

        }
        else
        {
            setLineRenderer(true);
        }
	}

    private void setLineRenderer(bool empty)
    {
        if (empty)
        {
            tempArray[0] = tempArray[1] = Vector2.zero;
        }
        else
        {
            tempArray[0] = lineRendererStart.position;
            tempArray[1] = this.transform.position;
        }
        lineRender.SetPositions(tempArray);
    }

    void OnMouseDown()
    {
        springElem.enabled = false;
        canDrag = true;
    }

    public void ResetScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    void DoDrag()
    {
        Vector3 mPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        mPos.z = 0;

        Vector2 dirSling = mPos-lineRendererStart.position;
        Vector2 dir = dirSling.normalized;

        Vector2 targetPos = new Vector2(lineRendererStart.position.x, lineRendererStart.position.y) + dir * Mathf.Clamp(dirSling.magnitude, minStretch, maxStretch);

        transform.position = targetPos;

    }

    void OnMouseUp()
    {
        springElem.enabled = true;
        rigid.isKinematic = false;
        canDrag = false;
    }
}
