﻿using UnityEngine;
using System.Collections;


[RequireComponent(typeof(SpriteRenderer))]
public class Box : MonoBehaviour {

    private SpriteRenderer sr;
	// Use this for initialization
	void Start () {
        sr = GetComponent<SpriteRenderer>();
        sr.color = new Color(Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f), Random.Range(0.0f, 1.0f));
	}
	

}
