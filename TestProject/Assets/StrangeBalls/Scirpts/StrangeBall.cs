﻿using UnityEngine;
using System.Collections;

public class StrangeBall : MonoBehaviour {

    private Rigidbody2D rigid;
    public float minForce = 10.0f;
    public float maxForce = 40.0f;
    public CircleCollider2D effector;
    public float force = 5.0f;
    public float maxVelMagnitude = 5.0f;
    public GameController gc;
    private bool flaged = false;
    public int ballsCount = 0;
    private bool freeForceActive = false;
    private float counter = 0;
    private CircleCollider2D selfCollider;
    // Use this for initialization
    void Start () {
        rigid = GetComponent<Rigidbody2D>();
        selfCollider = GetComponent<CircleCollider2D>();
        addRandomForce();
	}


    void Update()
    {
        if(freeForceActive)
        {
            counter += Time.deltaTime;
            if(counter>=0.5f)
            {
                freeForceActive = false;
                selfCollider.enabled = true;
                effector.enabled = true;
            }
        }

       
        if (rigid.velocity.magnitude > maxVelMagnitude)
        { 
            rigid.velocity = rigid.velocity.normalized * maxVelMagnitude;
        }
    }


    public void moveTo(StrangeBall sb)
    {
        Vector2 tempForce;
        tempForce = this.transform.position - sb.transform.position;
        tempForce.Normalize();
        if (gc.forceAwayActive)
        {
            sb.addForce(tempForce * -force);
        }
        else
        {
            sb.addForce(tempForce * force);
        }
    }


    void addForce(Vector2 f)
    {
        rigid.AddForce(f);
    }
        

    void addRandomForce(float mul=1.0f)
    {
        Vector2 dir = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
        dir.Normalize();

        float force = Random.Range(minForce, maxForce);
        rigid.AddForce(dir * force*mul);
     
    }

    public void addFreeForce()
    {
        freeForceActive = true;
        selfCollider.enabled = false;
        effector.enabled = false;
        addRandomForce(4.0f);

    }


    void OnCollisionEnter2D(Collision2D coll)
    {
        if (gc.forceAwayActive == true) return;
        flaged = true;
        StrangeBall sb = coll.transform.GetComponent<StrangeBall>();
        if (sb.force < force || sb.flaged == false)
        {
            force *= gc.growForceMul;
            Destroy(coll.transform.gameObject);

            Vector3 temp = transform.localScale;
            temp.x *= gc.growScaleMul;
            temp.y *= gc.growScaleMul;
            transform.localScale = temp;
            ballsCount++;
            if(ballsCount>=50)
            {
                gc.fiftyCreate(this.transform.position);
                gameObject.SetActive(false);
            }
        }
    }


}
