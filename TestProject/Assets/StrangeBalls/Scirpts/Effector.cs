﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Effector : MonoBehaviour {

    public StrangeBall controller;

    void OnTriggerStay2D(Collider2D col)
    {
        Effector ef=col.transform.GetComponent<Effector>();
        if(ef!=null)
        {
            if(ef.controller != controller)
            {
                ef.controller.moveTo(controller);
            }
        }
    }
	
}
